import React from 'react';
import {Button, View} from 'react-native';
import {createDrawerNavigator} from '@react-navigation/drawer';
import Stacknavigation from "./stacknav";
import Screen2 from '../screens/screen2';
import Screen3 from '../screens/screen3';

const Drawer = createDrawerNavigator();

const DrawerNav = () => {
  return (
    <Drawer.Navigator>
      <Drawer.Screen name="screen2" component={Stacknavigation} />
      <Drawer.Screen name="screen3" component={Screen3} />
    </Drawer.Navigator>
  );
};

export default DrawerNav;

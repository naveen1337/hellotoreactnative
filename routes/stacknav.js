import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
const Stack = createStackNavigator();

import Drawernavigation from "./drawer-root";

import Screen1 from '../screens/screen1';
import Screen2 from '../screens/screen2';


export default function StackNavigator() {
    return(
        <Stack.Navigator initialRouteName="screen1">
        <Stack.Screen name="screen1" component={Screen1} />
        <Stack.Screen name="screen2" component={Screen2} />
      </Stack.Navigator>
    );
 
}

import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,  
} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';

import Drawernavigation from "./routes/drawer-root";




const App = () => { 
  return (
      <NavigationContainer>
        <Drawernavigation/>
      </NavigationContainer>
  );
};

const styles = StyleSheet.create({});

export default App;

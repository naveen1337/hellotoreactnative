import React from "react"
import {View,Text} from "react-native"

const Screen2 = ({navigation,route}) =>{
    console.log(route)

    return(
        <View style={{flex:1, justifyContent:"center", alignItems:"center"}}> 
            <Text>Screen 2</Text>

            {/* <Text>{route.params.name}</Text> */}
        </View>
    )
}

export default Screen2
    
import React from "react"
import {View,Text, Button} from "react-native"

const Screen1 = ({navigation}) =>{
    return(
        <View style={{flex:1, justifyContent:"center", alignItems:"center"}}> 
            <Text>Screen 1</Text>
            <Button onPress={() =>navigation.navigate("screen2",{"name":"hello world"})} title="Go to Screen 2"></Button>
            <Button onPress={()=>navigation.openDrawer()} title="Open Drawer">  </Button>
        </View>
    );
}

export default Screen1